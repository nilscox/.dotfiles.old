#!/bin/sh

# File: setup.sh
# Author: Nils Layet <nils.layet@epita.fr>
# Description: setup script
# Created: 2018-03-20
# Modified: 2018-05-05

set -e

GIT_DIRCOLORS='git://github.com/seebi/dircolors-solarized'
GIT_OH_MY_ZSH='git://github.com/robbyrussell/oh-my-zsh.git'
GIT_VUNDLE_VIM='git://github.com/VundleVim/Vundle.vim.git'
GIT_ZSH_GIT_PROMPT='git://github.com/olivierverdier/zsh-git-prompt'

[ -z "$DISTRIB" ] && DISTRIB=$(grep 'ID=' /etc/os-release | awk -F= '{ print $2 }')
[ -z "$DOT" ] && DOT=$(dirname $(readlink -f "$0"))
[ -z "$DEST" ] && DEST="$HOME"

update() {
  if [ "$DISTRIB" == "arch" ] || [ "$DISTRIB" == "archarm" ]; then
    sudo pacman -Syu --noconfirm
  elif [ "$DISTRIB" == "debian" ]; then
    sudo apt-get update
  fi
}

install_pkg() {
  if [ "$DISTRIB" == "arch" ] || [ "$DISTRIB" == "archarm" ]; then
    sudo pacman -S $@ --noconfirm
  elif [ "$DISTRIB" == "debian" ]; then
    sudo apt-get install $@
  fi
}

clone_or_pull() {
  if [ -d "$2" ]; then
    (cd "$2" && git pull)
  else
    git clone --depth=1 "$1" "$2"
  fi
}

setup_zsh() {
  clone_or_pull "$GIT_OH_MY_ZSH" "$DEST/.oh-my-zsh"

  ln -sf "$DOT/zshrc" "$DEST/.zshrc"
  ln -sf "$DOT/oh-my-zshrc" "$DEST/.oh-my-zshrc"

  if [ "$DISTRIB" == "arch" ] || [ "$DISTRIB" == "archarm" ]; then
    ln -sf "$DOT/zshrc.archlinux" "$DEST/.zshrc.archlinux"
  elif [ "$DISTRIB" == "debian" ]; then
    ln -sf "$DOT/zshrc.debian" "$DEST/.zshrc.debian"
  fi

  mkdir -p "$DEST/.config"
  clone_or_pull "$GIT_DIRCOLORS" "$DEST/.config/dircolors-solarized"

  ln -sf "$DEST/.config/dircolors-solarized/dircolors.ansi-universal" "$DEST/.dir_colors"

  clone_or_pull "$GIT_ZSH_GIT_PROMPT" "$DEST/.config/zsh-git-prompt"
}

setup_ssh() {
  mkdir -p "$DEST/.ssh"
  ln -sf "$DOT/ssh.config" "$DEST/.ssh/config"

  if [ ! -f "$DEST/.ssh/id_ed25519" ]; then
    ssh-keygen -f "$DEST/.ssh/id_ed25519" -t ed25519
  fi
}

setup_git() {
  ln -sf "$DOT/gitconfig" "$DEST/.gitconfig"
  ln -sf "$DOT/gitignore_global" "$DEST/.gitignore_global"
}

setup_vim() {
  clone_or_pull "$GIT_VUNDLE_VIM" "$DEST/.vim/bundle/Vundle.vim"

  ln -sf "$DOT/vimrc" "$DEST/.vimrc"

  vim +PluginInstall +qall
}

setup_sublimetext() {
  PACKAGE_DIR="$DEST/.config/sublime-text-3/Packages"
  USER_DIR="$PACKAGE_DIR/User"

  mkdir -p "$PACKAGE_DIR"
  rm -rf "$USER_DIR"
  ln -s "$DOT/sublime" "$USER_DIR"
  ln -sf "$USER_DIR" "$DEST/.sublime"
}

setup_terminator() {
  mkdir -p "$DEST/.config/terminator"
  ln -sf "$DOT/terminator.config" "$DEST/.config/terminator/config"
}

set_shell_zsh() {
  if [ "$SHELL" != '/bin/zsh' ] && [ "$SHELL" != '/usr/bin/zsh' ]; then
    chsh -s /bin/zsh
  fi
}

main() {
  git pull "$DOT"

  # update
  # install_pkg zsh openssh git vim

  setup_zsh
  setup_ssh
  setup_git
  setup_vim
  setup_sublimetext
  setup_terminator

  set_shell_zsh
}

main
