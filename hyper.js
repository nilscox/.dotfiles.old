// Future versions of Hyper may add additional config options,
// which will not automatically be merged into this file.
// See https://hyper.is#cfg for all currently supported options.

const colors            = {
      black             : '#20242c',
      red               : '#BF616A',
      green             : '#A3BE8C',
      yellow            : '#EBCB8B',
      blue              : '#5E81AC',
      magenta           : '#B48EAD',
      cyan              : '#88C0D0',
      white             : '#c0c2c5',
      lightBlack        : '#4C566A',
      lightRed          : '#BF616A',
      lightGreen        : '#A3BE8C',
      lightYellow       : '#D08770',
      lightBlue         : '#77ABE7',
      lightMagenta      : '#CAA6EC',
      lightCyan         : '#8FBCBB',
      lightWhite        : '#96999f',
};

module.exports = {
  config: {
    // choose either `'stable'` for receiving highly polished,
    // or `'canary'` for less polished but more frequent updates
    updateChannel: 'stable',

    // the full list. if you're going to provide the full color palette,
    // including the 6 x 6 color cubes and the grayscale map, just provide
    // an array here instead of a color map object
    colors,

    // color of the text
    foregroundColor: colors.white,

    // terminal background color
    // opacity is only supported on macOS
    backgroundColor: colors.black,

    // border color (window, tabs)
    borderColor: colors.lightBlack,

    // terminal cursor background color and opacity (hex, rgb, hsl, hsv, hwb or cmyk)
    cursorColor: colors.lightWhite,

    // `'BEAM'` for |, `'UNDERLINE'` for _, `'BLOCK'` for █
    cursorShape: 'BEAM',

    // set to `true` (without backticks and without quotes) for blinking cursor
    cursorBlink: false,

    // default font size in pixels for all tabs
    fontSize: 12,

    // font family with optional fallbacks
    fontFamily: '"Fira Code", Menlo, "DejaVu Sans Mono", Consolas, "Lucida Console", monospace',

    // default font weight: 'normal' or 'bold'
    fontWeight: 'normal',

    // font weight for bold characters: 'normal' or 'bold'
    fontWeightBold: 'bold',

    // terminal selection color
    selectionColor: 'rgba(0, 0, 0, 0.8)',

    // set to `true` (without backticks and without quotes) if you're using a
    // Linux setup that doesn't show native menus
    // default: `false` on Linux, `true` on Windows, ignored on macOS
    showHamburgerMenu: false,

    // set to `false` (without backticks and without quotes) if you want to hide the minimize, maximize and close buttons
    // additionally, set to `'left'` if you want them on the left, like in Ubuntu
    // default: `true` (without backticks and without quotes) on Windows and Linux, ignored on macOS
    showWindowControls: false,

    // custom padding (CSS format, i.e.: `top right bottom left`)
    padding: '6px 4px',

    // custom CSS to embed in the main window
    css: `
      .notifications_view {
        animation: fadeOut ease 5s;
        animation-fill-mode: forwards;
      }

      @keyframes fadeOut {
        80% {
          opacity: 1;
        }
        99% {
          dispaly: block;
        }
        100% {
          display: none;
          opacity: 0;
        }
      }

      .term_fit:not(.term_term):not(.term_active) {
        background: #16161c;
      }
    `,

    // custom CSS to embed in the terminal window
    termCSS: `
      ::selection {
          background: ${colors.lightWhite} !important;
      }

      .cursor-node[focus=true]:not([hyper-blink-moving]) {
          animation: blink 0.8s ease infinite;
      }

      @keyframes blink {
          50% { opacity: 0.5; }
      }
    `,

    // the shell to run when spawning a new session (i.e. /usr/local/bin/fish)
    // if left empty, your system's login shell will be used by default
    //
    // Windows
    // - Make sure to use a full path if the binary name doesn't work
    // - Remove `--login` in shellArgs
    //
    // Bash on Windows
    // - Example: `C:\\Windows\\System32\\bash.exe`
    //
    // PowerShell on Windows
    // - Example: `C:\\WINDOWS\\System32\\WindowsPowerShell\\v1.0\\powershell.exe`
    shell: '',

    // for setting shell arguments (i.e. for using interactive shellArgs: `['-i']`)
    // by default `['--login']` will be used
    shellArgs: ['--login'],

    // for environment variables
    env: {},

    // set to `false` for no bell
    bell: false,

    // if `true` (without backticks and without quotes), selected text will automatically be copied to the clipboard
    copyOnSelect: false,

    // if `true` (without backticks and without quotes), hyper will be set as the default protocol client for SSH
    defaultSSHApp: true,

    // if `true` (without backticks and without quotes), on right click selected text will be copied or pasted if no
    // selection is present (`true` by default on Windows and disables the context menu feature)
    // quickEdit: true,

    // URL to custom bell
    // bellSoundURL: 'http://example.com/bell.mp3',

    // for advanced config flags please refer to https://hyper.is/#cfg

    paneNavigation: {
      debug: false,
      hotkeys: {
        navigation: {
          up: 'alt+up',
          down: 'alt+down',
          left: 'alt+left',
          right: 'alt+right'
        },

        // completed with 1-9 digits
        jump_prefix: 'ctrl+alt',

        // Added to jump and navigation hotkeys for pane permutation
        permutation_modifier: 'shift',

        maximize: 'alt+enter'
      },
      // Show pane number
      showIndicators: false,

      // Will be completed with pane number
      indicatorPrefix: '^⌥',

      // Added to indicator <div>
      indicatorStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        fontSize: '10px'
      },
      focusOnMouseHover: false
    }
  },

  // a list of plugins to fetch and install from npm
  // format: [@org/]project[#version]
  // examples:
  //   `hyperpower`
  //   `@company/project`
  //   `project#1.0.1`
  plugins: [
    'hyper-pane',
  ],

  // in development, you can create a directory under
  // `~/.hyper_plugins/local/` and include it here
  // to load it and avoid it being `npm install`ed
  localPlugins: [],

  keymaps: {
    // Example
    // 'window:devtools': 'cmd+alt+o',

    'pane:splitVertical': 'alt+v',
    'pane:splitHorizontal': 'alt+h',
  },
};
